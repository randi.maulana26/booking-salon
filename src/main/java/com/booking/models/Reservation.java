package com.booking.models;

import java.util.List;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Reservation {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;
    private static int countID = 1;
    //   workStage (In Process, Finish, Canceled)

    public Reservation(Customer customer, Employee employee, List<Service> services,
            String workstage) {
        this.reservationId = generateResID();
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.reservationPrice = calculateReservationPrice();
        this.workstage = workstage;
        countID++;

    }

    public String generateResID(){
        if (countID < 10) {
            return "Rsv-0" + countID;
        }else {
            return "Rsv-" + countID;
        }
    }

    public double calculateReservationPrice() {
        String membership;
        membership = customer.getMember().getMembershipName();
        double reservationPrice = 0.0;
        for (Service service : services) {
            reservationPrice += service.getPrice();
        }
        double hasil;
        switch (membership) {
            case "Silver":
                hasil = reservationPrice * 0.95;
                break;
            case "Gold":
                hasil = reservationPrice * 0.90;
                break;
            default:
                hasil = reservationPrice;
        }
        return hasil;
    }
}
