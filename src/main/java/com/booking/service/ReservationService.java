package com.booking.service;


import com.booking.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.booking.service.ValidationService.*;

public class ReservationService {
    public static void createReservation(List<Person> personList, List<Service> serviceList, List<Reservation> reservationList, Scanner input) {
        String inputOpt = "Y";
        do {
            System.out.println("Ingin Melakukan Reservasi? (Y/N)");
            inputOpt = input.nextLine();
            if (inputOpt.equalsIgnoreCase("Y")) {
                Customer customer = selectCustomer(personList, input);
                Employee employee = selectEmployee(personList, input);
                List<Service> selectedServices = selectServices(serviceList, input);
                String workstage = "In Process";
                Reservation reservation = new Reservation(customer, employee, selectedServices, workstage);
                reservation.calculateReservationPrice();
                reservationList.add(reservation);
                System.out.println("Reservasi berhasil");
            }
        } while (!inputOpt.equalsIgnoreCase("N"));
    }

    public static Customer selectCustomer(List<Person> personList, Scanner input) {
        String customerId = null;
        do {
            PrintService.showAllCustomer(personList);
            System.out.println("Silahkan masukan Customer id: ");
            customerId = input.nextLine();
            if (findCustomerId(customerId, personList) == null) {
                System.out.println("Customer yang dicari tidak tersedia");
            }
        } while (findCustomerId(customerId, personList) == null);

        return (Customer) findCustomerId(customerId, personList);
    }

    public static Employee selectEmployee(List<Person> personList, Scanner input) {
        String employeeId = null;
        do {
            PrintService.showAllEmployee(personList);
            System.out.println("Silahkan masukan Employee id: ");
            employeeId = input.nextLine();
            if (findEmployeeId(employeeId, personList) == null) {
                System.out.println("Employee ID tidak valid!");
            }
        } while (findEmployeeId(employeeId, personList) == null);

        return (Employee) findEmployeeId(employeeId, personList);
    }

    public static List<Service> selectServices(List<Service> serviceList, Scanner input) {
        List<Service> selectedServices = new ArrayList<>();
        String inputOpt = "Y";
        do {
            PrintService.showAllServices(serviceList);
            System.out.println("Masukkan ID Service yang dipilih: ");
            String serviceId = input.nextLine();
            Service selectedService = findServiceById(serviceId, serviceList);
            if (selectedService != null && !selectedServices.contains(selectedService)) {
                selectedServices.add(selectedService);
            } else {
                System.out.println("Service dengan ID tersebut tidak valid atau sudah dipilih.");
            }
            System.out.println("Ingin memilih service lain? (Y/T)");
            inputOpt = input.nextLine();
        } while (inputOpt.equalsIgnoreCase("Y"));
        return selectedServices;
    }

    public static void finishOrCancelReservation(List<Reservation> reservationList, Scanner input) {
        System.out.println("Silahkan Masukkan Reservation Id: ");
        String reservationId = input.nextLine();
        Reservation reservation = findReservationById(reservationId, reservationList);

        if (reservation != null) {
            System.out.println("Selesaikan reservasi (Finish) atau batalkan reservasi (Cancel): ");
            String action = input.nextLine();
            if (action.equalsIgnoreCase("Finish")) {
                reservation.setWorkstage("Finished");
                System.out.println("Reservasi dengan id " + reservationId + " sudah Finish");
            } else if (action.equalsIgnoreCase("Cancel")) {
                reservationList.remove(reservation);
                System.out.println("Reservasi dengan id " + reservationId + " sudah dibatalkan");
            } else {
                System.out.println("Aksi tidak valid. Silahkan coba lagi.");
            }
        } else {
            System.out.println("Reservasi dengan id " + reservationId + " tidak ditemukan.");
        }
    }

    private static Reservation findReservationById(String reservationId, List<Reservation> reservationList) {
        for (Reservation reservation : reservationList) {
            if (reservation.getReservationId().equalsIgnoreCase(reservationId)) {
                return reservation;
            }
        }
        return null;
    }
    public static void showReservationHistory(List<Reservation> reservationList) {
        if (reservationList.isEmpty()){
            System.out.println("===================Data tidak ada==================");
            return;
        }
        double totalKeuntungan = 0.0;

        System.out.printf("| %-4s | %-8s | %-20s | %-30s | %-12s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
        System.out.println("-------------------------------------------------------------------------------------------");

        int index = 1;
        for (Reservation reservation : reservationList) {
            StringBuilder services = new StringBuilder();
            for (Service service : reservation.getServices()) {
                if (services.length() > 0) {
                    services.append(", ");
                }
                services.append(service.getServiceName());
            }

            System.out.printf("| %-4d | %-8s | %-20s | %-30s | %-10.2f | %-10s |\n",
                    index, reservation.getReservationId(), reservation.getCustomer().getName(),
                    services.toString(), reservation.getReservationPrice(), reservation.getWorkstage());

            if ("Finished".equalsIgnoreCase(reservation.getWorkstage())) {
                totalKeuntungan += reservation.getReservationPrice();
            }
            index++;
        }

        System.out.println("-------------------------------------------------------------------------------------------");
        System.out.printf("Total Keuntungan: \t\t\t\t\t\t\t\t\t\t%.2f\n", totalKeuntungan);
    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
