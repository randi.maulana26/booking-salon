package com.booking.service;

import java.util.List;

import com.booking.models.*;

public class PrintService {
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
    }

    public static void showAllServices(List<Service> serviceList){
        int num = 1;
        System.out.printf("| %-4s | %-8s | %-20s | %-15s |\n",
                "No.", "ID", "Service Name", "Biaya");
        System.out.println("+========================================================================================+");
        for (Service service : serviceList) {
            System.out.printf("| %-4s | %-8s | %-20s  | %-15s |\n",
                    num, service.getServiceId(), service.getServiceName(), service.getPrice());
            num++;
        }
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showRecentReservation(List<Reservation> reservationList){
        if (reservationList.isEmpty()){
            System.out.println("===================Data tidak ada==================");
            return;
        }

        int num = 1;
        System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Total Biaya", "Workstage");
        System.out.println("+========================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-4s | %-11s  | %-15s | %-10s |\n",
                num, reservation.getReservationId(), reservation.getCustomer().getName(), reservation.getReservationPrice(), reservation.getWorkstage());
                num++;
            }
        }
    }

    public static void showAllCustomer(List<Person> personList){
        int i =1;
        System.out.println("------------------------------------------------------------------------");
        System.out.printf("%-6s %-15s %-15s %-20s %-10s", "No", "ID", "Nama", "Alamat", "Membership");
        System.out.println();
        System.out.println("------------------------------------------------------------------------");
        for (Person pers : personList) {
            if (pers instanceof Customer) {
                Customer customer = (Customer) pers;
                System.out.printf("%-6s %-15s %-15s %-20s %-10s",
                        i, customer.getId(), customer.getName(), customer.getAddress(), customer.getMember().getMembershipName());
                System.out.println();
                i++;

            }
        }
        System.out.println("------------------------------------------------------------------------");
    }

    public static void showAllEmployee(List<Person> personList){
        int i = 1;
        System.out.println("------------------------------------------------------------------------");
        System.out.printf("%-6s %-15s %-15s %-20s %-10s", "No", "ID", "Nama", "Alamat", "Pengalaman");
        System.out.println();
        System.out.println("------------------------------------------------------------------------");
        for (Person person : personList){
            if (person instanceof Employee){
                Employee employee = (Employee) person;
                System.out.printf("%-6s %-15s %-15s %-20s %-10s",
                        i,employee.getId(),employee.getName(),employee.getAddress(),employee.getExperience());
                System.out.println();
                i++;
            }
        }
        System.out.println("------------------------------------------------------------------------");
    }
    }
