package com.booking.service;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Service;

import java.util.List;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
    public static Person findCustomerId(String customerId, List<Person> personList) {
        for (Person person : personList) {
            if (person instanceof Customer && person.getId().equals(customerId)) {
                return person;
            }
        }
        return null;
    }

    public static Person findEmployeeId(String employeeId, List<Person> personList) {
        for (Person person : personList) {
            if (person instanceof Employee && person.getId().equals(employeeId)) {
                return person;
            }
        }
        return null;
    }

    public static Service findServiceById(String serviceId, List<Service> serviceList) {
        for (Service service : serviceList) {
            if (service.getServiceId().equals(serviceId)) {
                return service;
            }
        }
        return null;
    }
}
